<?php
class ArtificialIntelligence {
	static function getComputerMove ($game) {
		$game->getPlayerBot ();
		if ($game->isPlayerOneBot && ($game->checkEvenOdd() == true)) {
			$freeFields = array_diff_key($game->field, $game->moves);
			$i = key($freeFields);
			$game->moves[$i] = "A";
		}
		if ($game->isPlayerTwoBot && ($game->checkEvenOdd() == false)) {
			$freeFields = array_diff_key($game->field, $game->moves);
			$i = key($freeFields);
			$game->moves[$i] = "B";
		}
		return $game;
	}
}
