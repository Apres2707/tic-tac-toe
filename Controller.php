<?php
class Controller {
	public function gamePlay() {
		if ($_POST["button"]) {
			$gamerFirst = new Gamer($_POST["name1"], $_POST["password1"]);
			$gamerSecond = new Gamer($_POST["name2"], $_POST["password2"]);
			$game = new Game($gamerFirst, $gamerSecond);
			$invalidName = Verification::checkName ($game);
			$id = $game->currentGameId;
			$dataToLoad = ["games", "moves", $id];
			$data = Storage::load ($dataToLoad);
			if ($data == true) {
				$data = unserialize($data);
			} else {
				$data = array();
			}
			$game->loadDateFromStorage($data);
			$game = $game -> addNewMove ();
			$winner = $game->checkWinner ();
			$draw = $game->checkDraw ();
			if (!$winner && !$draw) {
				$game = ArtificialIntelligence::getComputerMove ($game);
				$winner = $game->checkWinner ();
				$draw = $game->checkDraw ();
			}
			$game->getMovesOnField();
			$dataToStorage = $game->getDataToStorage();
			Storage::save ($dataToStorage);
			if ($invalidName) {
				View::getNameWithError ($invalidName);
			} elseif ($winner) {
				View::getResult ($winner);
			} elseif ($draw == true) {
				View::getDrawResault ();
			} else {
				$currentPlayer = $game->checkCurrentPlayer ();
				View::getField ($currentPlayer, $game);
			}
		}
		else {
			View::registerPlayers ();
		}
	}
}
