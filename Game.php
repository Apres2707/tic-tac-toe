<?php
class Game implements Storegable {
	public static $player1;
	public static $player2;
	public static $currentGameId;
	public $isPlayerOneBot;
	public $isPlayerTwoBot;
	public $moves;
	public $field = [0, 1, 2, 3, 4, 5, 6, 7, 8];
	private $newMove;
	public function __construct ($gamerFirst, $gamerSecond) {
		$this->player1 = $gamerFirst;
		$this->player2 = $gamerSecond;
		$this->newMove = $_POST["field"];
		$this->getModelIdentifier();
	}
	public function getPlayerBot () {
		if ($this->player1->playerName == "ПК") {
			$this->isPlayerOneBot = true;
		}
		if ($this->player2->playerName == "ПК") {
			$this->isPlayerTwoBot = true;
		}
	}
	public function checkEvenOdd () {
		if ((array_count_values($this->moves)["A"] + array_count_values($this->moves)["B"])%2 == 0) {
			return true;
		} else {
			return false;
		}
	}
	public function addNewMove () {
		if (isset($this->newMove) && $this->moves[$this->newMove] != "A" && $this->moves[$this->newMove] != "B") {
			if ($this->checkEvenOdd()) {
				$this->moves[$this->newMove] = "A";
			} else {
				$this->moves[$this->newMove] = "B";
			}
		}
		return $this;
	}
	public function checkWinner () {
		$win = [$this->moves[0].$this->moves[1].$this->moves[2], $this->moves[3].$this->moves[4].$this->moves[5], $this->moves[6].$this->moves[7].$this->moves[8], $this->moves[0].$this->moves[3].$this->moves[6], $this->moves[1].$this->moves[4].$this->moves[7], $this->moves[2].$this->moves[5].$this->moves[8], $this->moves[0].$this->moves[4].$this->moves[8], $this->moves[2].$this->moves[4].$this->moves[6]];
		foreach ($win as $key => $value) {
			if ($value == "AAA") {
				return $this->player1->playerName;
				break;
			} elseif ($value == "BBB") {
				return $this->player2->playerName;
				break;
			}
		}
	}
	public function checkDraw () {
		$movesTotal = array_count_values($this->moves)["A"] + array_count_values($this->moves)["B"];
		if ($movesTotal == 9) {
			return true;
		} else {
			return false;
		}
	}
	public function getMovesOnField () {
		$this->field = array_replace($this->field, $this->moves);
		return true;
	}
	public function checkCurrentPlayer () {
		if ($this->checkEvenOdd()) {
			$player = $this->player1->playerName;
		} else {
			$player = $this->player2->playerName;
		}
		return $player;
	}
	public function getModelIdentifier() {
		$paramArray = ["games", "player1", "player2", $this->player1->playerId, $this->player2->playerId];
		$this->currentGameId = Storage::getCurrentId ($paramArray);
		return true;
	}
	public function loadDateFromStorage(array $data):bool {
		$this->moves = $data;
		return true;
	}
	public function getDataToStorage():array {
		$movesEnd = serialize($this->moves);
		$dataToStorage = ["games", "moves", $movesEnd, $this->currentGameId];
		return $dataToStorage;
	}
}
