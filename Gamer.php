<?php
class Gamer implements Storegable {
	public $playerName;
	public $playerId;
	public $password;
	public function __construct ($name, $password) {
		$this->playerName = $name;
		$this->password = $password;
		$this->getModelIdentifier();
	}
	public function getModelIdentifier():string {
		$paramArray = ["players", "name", "password", $this->playerName, $this->password];
		$this->playerId = Storage::getCurrentId ($paramArray);
		return true;
	}
	public function loadDateFromStorage(array $data):bool {
		$this->playerName = $data;
		return true;
	}
	public function getDataToStorage():array {
		$dataToStorage = ["players", "name", $this->playerName, $this->playerId];
		return $dataToStorage;
	}
}
