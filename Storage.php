<?php
class Storage implements iStorage {
	static $dns="pgsql:host='127.0.0.1';dbname=test";
	static $user = "apres";
	static $pass = "aaaa1304";
	static function getPDO () {
		$db = new PDO (self::$dns, self::$user, self::$pass);
		return $db;
	}
	public function getCurrentId ($paramArray) {
		$table = array_shift($paramArray);
		$param1 = array_shift($paramArray);
		$param2 = array_shift($paramArray);
		$getId=self::getPDO()->prepare("SELECT id FROM $table WHERE $param1=? AND $param2=?");
		$getId->execute($paramArray);
		$id=$getId->fetch(PDO::FETCH_NUM);
		$currentId = $id[0];
		if (!$currentId) {
			$put=self::getPDO()->prepare("INSERT INTO $table ($param1, $param2) VALUES (?, ?)");
			$put->execute($paramArray);
		}
		$getId->execute($paramArray);
		$id=$getId->fetch(PDO::FETCH_NUM);
		$currentId = $id[0];
		return $currentId;
	}
	public function load ($dataToLoad) {
		$table = array_shift($dataToLoad);
		$cell = array_shift($dataToLoad);
		$data = self::getPDO()->prepare("SELECT $cell FROM $table WHERE id=?");
		$data->execute($dataToLoad);
		$dataFromStorage = $data->fetch(PDO::FETCH_NUM);
		$dataFromStorage = $dataFromStorage[0];
		return $dataFromStorage;
	}
	public function save ($dataToStorage): bool {
		$table = array_shift($dataToStorage);
		$cell = array_shift($dataToStorage);
		$data = self::getPDO()->prepare("UPDATE $table SET $cell=? WHERE id=?");
		$data->execute($dataToStorage);
		return true;
	}
}
