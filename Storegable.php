<?php
interface Storegable {
/**
* получение данных для сохранения в базу
* @returns array одномерный массив данных, ключи - название параметра, значение элемиента массива - значение параметра, например ['paramName'=> 'value']
*/ 
public function getDataToStorage():array;

/**
* Загрузка данных из базы в модель
* @param array $data данные полученные из хранилища
* @see self::getDataToStorage()
*/
public function loadDateFromStorage(array $data):bool;

/**
* Получение уникального идентификатора корзины (папки, таблицы и прочего) 
*/ 
public function getModelIdentifier();//:string;
}
