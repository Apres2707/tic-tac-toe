<?php
class Verification {
	static function searchErrorInName ($name) {
		if (preg_match("/[^\w\s]/u", $name)) {
			return true;
		} else {
			return false;
		}
	}
	public function checkName ($game) {
		if ($_POST["button"] && self::searchErrorInName ($game->player1->playerName)) {
			$nameWithError = "X";
			return $nameWithError;
		} elseif ($_POST["button"] && self::searchErrorInName ($game->player2->playerName)) {
			$nameWithError = "O";
			return $nameWithError;
		} else {
			return false;
		}
	}
}
