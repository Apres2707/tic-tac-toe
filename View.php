<?php
class View {
	public static $newGame = '<form name="game" action="/"><p><input type="submit" name="button" value="Новая игра"></p></form>';
	// public static $newGameWithBot = '<form name="game" action="/"><p><input type="submit" name="button" value="Играть X с ботом"><input type="submit" name="button" value="Играть O с ботом"></p></form>';
	public static function getResult ($winner) {
		echo "<h2>Игрок ".$winner." победил!</h2>".self::$newGame;
	}
	public static function getDrawResault () {
		echo "<h2>Ничья!</h2>".self::$newGame;
	}
	public static function registerPlayers () {
		echo '<html><form action="/" method="post">Имя игрока X <input type="text" name="name1" required><br />Пароль игрока X <input type="text" name="password1" required><br /><br />Имя игрока O <input type="text" name="name2" required><br />Пароль игрока O <input type="text" name="password2" required><br /><br /><input type="submit" name="button" value="OK"></form></html>';
	}
	// public static function registerPlayerWithBotX () {
	// 	echo '<html><form action="/" method="post"><input type="hidden" name="name1" value="ПК"><input type="hidden" name="password1" value="ПК">Имя игрока O <input type="text" name="name2" required><br />Пароль игрока O <input type="text" name="password2" required><br /><br /><input type="submit" name="button" value="OK"></form></html>'.self::$newGame;
	// }
	// public static function registerPlayerWithBotO () {
	// 	echo '<html><form action="/" method="post">Имя игрока X <input type="text" name="name1" required><br />Пароль игрока X <input type="text" name="password1" required><input type="hidden" name="name2" value="ПК"><input type="hidden" name="password2" value="ПК"><br /><br /><input type="submit" name="button" value="OK"></form></html>'.self::$newGame;
	// }
	public static function getNameWithError ($nameWithError) {
		echo '<html><h2>Имя игрока '.$nameWithError.' заполнено некорректно!</h2><form method="POST" name="game" action="/"><p><input type="submit" value="Назад"></p></form></html>';
	}
	public static function getField ($currentPlayer, $game) {
		foreach ($game->field as $key => &$value) {
			if ($value === "A") {
				$value = "X";
			} elseif ($value ==="B") {
				$value = "O";
			} else {
				$value = "<input name='field' type='radio' value='$value'>";
			}
		}
		unset($value);
		echo '<html><h1>Ход игрока '.$currentPlayer.'</h1><form method="POST" name="game" action="/"><input name="name1" type="hidden" value="'.$game->player1->playerName.'"><input name="password1" type="hidden" value="'.$game->player1->password.'"><input name="name2" type="hidden" value="'.$game->player2->playerName.'"><input name="password2" type="hidden" value="'.$game->player2->password.'"><table border="1" cellpadding="10"><tr><td>'.$game->field[0].'</td><td>'.$game->field[1].'</td><td>'.$game->field[2].'</td></tr><tr><td>'.$game->field[3].'</td><td>'.$game->field[4].'</td><td>'.$game->field[5].'</td></tr><tr><td>'.$game->field[6].'</td><td>'.$game->field[7].'</td><td>'.$game->field[8].'</td></tr></table><p><input type="submit" name="button" value="Сделать ход"></p></form></html>';
	}
}
