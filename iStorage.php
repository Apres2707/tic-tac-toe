<?php
interface iStorage {
	public function save ($dataToStorage): bool;
	public function load ($currentGameId);//: string;
	// public function list (string $folder): array; // [­['id' => '', 'name' => '']...]
	public function getCurrentId ($gamerArray);//: string;
}
